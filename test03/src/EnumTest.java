import java.util.Scanner;

/**
 *
 * Created by jyo on 15-11-7.
 */
public class EnumTest {
    public static void main(String[] args) {
        String log4jConfPath = "log4j.properties";
        org.apache.log4j.PropertyConfigurator.configure(log4jConfPath);
        Scanner in = new Scanner(System.in);
        System.out.println("Enter a Size : (SMALL,MEDIUM,LARGE,EXTRA_LARGE)");
        String input = in.next().toUpperCase();

        // 对Size这个枚举类赋一个常量
        Size size = Enum.valueOf(Size.class,input);

        System.out.println("abbrevation="+size.getAbbreviation());
        if(size == Size.EXTRA_LARGE)
            System.out.println("Good job -- you paid attention to the _.");



    }
}

enum Size{
    SMALL("S"),MEDIUM("M"),LARGE("L"),EXTRA_LARGE("XL");

    private String abbreviation;

    Size(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getAbbreviation() {
        return abbreviation;
    }
}