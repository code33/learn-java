package p1;

/**
 * Created by jyo on 15-11-5.
 */
public class Manager extends Employee {
    public Manager(String name) {
        super(name);
    }

    // 子类构造器形参个数不得少于父类
    public Manager(String name, String authority, String educational) {
        super(name);
        this.authority = authority;
        this.educational = educational;
    }

    private String authority;

    private String educational;

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public String getEducational() {
        return educational;
    }

    public void setEducational(String educational) {
        this.educational = educational;
    }

    @Override
    public String toString() {
        return "Manager{" +
                "authority='" + authority + '\'' +
                ", educational='" + educational + '\'' +
                '}';
    }
}
