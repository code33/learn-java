package expend;

import org.apache.log4j.PropertyConfigurator;

/**
 * Created by jyo on 15-11-6.
 */
public class Demo {
    public static void main(String[] args) {
        String log4jConfPath = "log4j.properties";
        PropertyConfigurator.configure(log4jConfPath);
        Dog dog = new Dog("Dodo",5);
        dog.say();
        dog.move(100);
    }
}

class Animal {
    protected String name;
    private String desc = "Animals are human's good friends";
    // 必须要声明一个 getter 方法
    public String getDesc() { return desc; }

    public void move(){
        System.out.println("Animals can move");
        name = "Dog";
    }

    public Animal(String name) {
        this.name = name;
    }
}

class Dog extends Animal{

    private int age;


    public void move(int length){
//        super.move();  // 调用父类的方法
        System.out.println("Dogs can walk and run");
        // 通过 getter 方法调用父类隐藏变量

        String desc= super.getDesc();
        System.out.println("Please remember: " + desc);
        System.out.println("Animal.name:" + super.name);
    }

    public Dog(String name, int age) {
        super(name);
        this.age = age;
    }

    public void say(){
        System.out.println("I'm "+this.name+" my age is "+this.age);
    }
}