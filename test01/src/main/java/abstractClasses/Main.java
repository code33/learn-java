package abstractClasses;

import org.apache.log4j.PropertyConfigurator;

import java.util.ArrayList;

/**
 *
 * Created by jyo on 15-11-5.
 */
public class Main {
    public static void main(String[] args) {
        String log4jConfPath = "log4j.properties";
        PropertyConfigurator.configure(log4jConfPath);

        int a=128;
        Person[] persons=new Person[a];
        persons[0]=new Employee("Jack",5000,1989,10,1);
        persons[1]=new Student("Lucky","English");

        // 被Overwrite过的方法,父类对象所调用的为子类的方法
        System.out.println("persons[0]:"+persons[0].getDescription());
        System.out.println("persons[1]:" + persons[1].getDescription());
        System.out.println("persons.length:" + persons.length);

        ArrayList<Person> persons1=new ArrayList<Person>();
        persons1.add(0, new Employee("Jim", 5000, 1989, 10, 1));
        persons1.add(1, new Student("Jyo", "Math"));
        persons1.set(0,new Employee("Jekin", 5000, 1989, 10, 1));
//        System.out.println("persons1.ArrayList:");
        for (Person p : persons1)
            System.out.println("persons1:"+p.getDescription());

    }
}
