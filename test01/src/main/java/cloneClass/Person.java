package cloneClass;

/**
 * Created by jyo on 15-11-8.
 */
public class Person  implements Cloneable {
    private int age;
    private String firstName;

    public Person clone() throws CloneNotSupportedException{
        Person cloned = (Person) super.clone();
//        firstName= cloned.firstName;
        return cloned;
    }

    public Person(int age, String firstName) {
        this.age = age;
        this.firstName = firstName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

//    @Override
//    public String toString() {
//        return "Person{" +
//                "age=" + age +
//                ", firstName='" + firstName + '\'' +
//                '}';
//    }
}
