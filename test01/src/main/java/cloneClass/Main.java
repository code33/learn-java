package cloneClass;




import com.github.underscore.$;
import com.rits.cloning.Cloner;
import org.apache.log4j.PropertyConfigurator;

/**
 * Created by jyo on 15-11-8.
 */
public class Main {
    public static void main(String[] args) {
        String log4jConfPath = "log4j.properties";
        PropertyConfigurator.configure(log4jConfPath);
        Person p = new Person(23, "zhang");
        Person p1 = null;

        p1= (Person) $.clone(p);
        p1.setFirstName("zhou");



        String result = p.getFirstName() == p1.getFirstName() ? "clone是浅拷贝的" : "clone是深拷贝的";
        System.out.println("result: " +result);
        System.out.println(p.getFirstName());
        System.out.println(p1.getFirstName());
        /*try {
            Employee original= new Employee("John",50000);
            original.setHireday(1999,12,1);
            Employee copy = original.clone();
            copy.raiseSalary(10);
            copy.setHireday(2002, 12, 1);
            System.out.println("original=" + original);
            System.out.println("copy=" +copy    );

        } catch (Exception e) {
            System.out.println("e:" +e);
            e.printStackTrace();
        }*/
    }
}
