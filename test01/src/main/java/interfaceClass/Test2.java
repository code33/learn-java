package interfaceClass;

/**
 * Created by jyo on 15-11-8.
 */
import java.util.Arrays;
import java.util.Comparator;
public class Test2{
    public static void main(String args[]){
        StudentScore[] ss = {new StudentScore("z3",23,105),new StudentScore("z1",22,101),new StudentScore("z2",20,105),new StudentScore("z4",22,104)};
        Arrays.sort(ss, new StudentCmp());
        for(StudentScore s : ss){
            System.out.println(s);
        }
    }
}
/*对象比较类,按分数从高到低，分数相等时按年龄从低到高排列*/
class StudentCmp implements Comparator<StudentScore>{
    @Override
    public int compare(StudentScore o1, StudentScore o2) {
        if(o1.getScore() < o2.getScore()){
            return 1;
        }else if(o1.getScore() > o2.getScore()){
            return -1;
        }else{
            if(o1.getAge() > o2.getAge()){
                return 1;
            }else if(o1.getAge() < o2.getAge()){
                return -1;
            }else{
                return 0;
            }
        }
    }

}

class StudentScore{
    private int score;
    private int age;
    private String name;

    public StudentScore(String name, int age, int score){
        this.name = name;
        this.age = age;
        this.score = score;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "StudentScore [name=" + name + ", age=" + age + ", score="
                + score + "]";
    }

}