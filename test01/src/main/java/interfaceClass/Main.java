package interfaceClass;

import org.apache.log4j.PropertyConfigurator;

import java.util.Arrays;

/**
 *
 * Created by jyo on 15-11-8.
 */
public class Main {
    public static void main(String[] args) {
        String log4jConfPath = "log4j.properties";
        PropertyConfigurator.configure(log4jConfPath);
        Employee[] staff = new Employee[3];
        staff[0]=new Employee("Hank",15000);
        staff[1]=new Employee("Hank",50000);
        staff[2]=new Employee("Tonye",38800);

        Arrays.sort(staff);
        for (Employee p:staff){
            System.out.println("name=" +p.getName()+" , salary="+p.getSalary());
        }
    }



}
