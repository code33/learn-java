package Calendar;

import java.lang.reflect.Array;
import java.text.DateFormatSymbols;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * Created by jyo on 15-8-29.
 */
public class CalendarTest {

    public static void main(String[] args){

        GregorianCalendar d=new GregorianCalendar();

        int today   =   d.get(Calendar.DAY_OF_MONTH);
        int month   =   d.get(Calendar.MONTH);

//      set d to start date of month
//      将这个日期对象设置为此月的首日 就是1号
        d.set(Calendar.DAY_OF_MONTH, 1);
        int thisDay = d.get(Calendar.MONTH);
//        System.out.printf("thisDay:%s",thisDay);
//      周几
        int weekday =   d.get(Calendar.DAY_OF_WEEK);

//      获取一周的首日 - 一般以周日作为起始
        int firstDayOfWeek  =   d.getFirstDayOfWeek();

        int indent  =   0;

        while (weekday != firstDayOfWeek){
            indent++;
//          到前一个月
            d.add(Calendar.DAY_OF_MONTH,-1);
            weekday =d.get(Calendar.DAY_OF_WEEK);
        }

        String[] weekdayNames   =   new DateFormatSymbols().getShortWeekdays();

        do{
            System.out.printf("%4s",weekdayNames[weekday]);
            d.add(Calendar.DAY_OF_MONTH, 1);
            weekday=d.get(Calendar.DAY_OF_WEEK);
        }
        while (weekday != firstDayOfWeek);

        System.out.println();
        String space="";
        System.out.print(" ");

        for (int i = 0;i<=indent;i++)
            System.out.printf("%5s",space);

        d.set(Calendar.DAY_OF_MONTH,1);
        do {
            int day =d.get(Calendar.DAY_OF_MONTH);
            System.out.printf("%5d", day);
            if (day == today) 
                System.out.print("*");
            else
                System.out.printf(" ");
            
            d.add(Calendar.DAY_OF_MONTH,1);;
            weekday=d.get(Calendar.DAY_OF_WEEK);
            if (weekday == firstDayOfWeek) System.out.println("");
        }
        while (d.get(Calendar.MONTH)==month);
        
        if(weekday!=firstDayOfWeek) System.out.println("");
    }
}
