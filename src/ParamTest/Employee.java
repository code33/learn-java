package ParamTest;

/**
 *
 * Created by jyo on 15-8-30.
 */
public class Employee {
    private String name;
    private double salary;

    public Employee(String n,double s){
        this.name=n;
        this.salary=s;
    }

    public double getSalary() {
        return salary;
    }

    public String getName() {
        return name;
    }

    public  void raiseSalary(double byPercent){
        double raise = salary * byPercent / 100;
        salary+=raise;
    }
}
