package ParamTest;
/**
 *
 * Created by jyo on 15-8-30.
 */
public class ParamTest {
    public static void main(String[] args) {
        /*
        *   Test 1 :Methods can't modify numeric parameters
        *
        * */
        System.out.println("Testing tripleValue:");
        double percent=10;
        System.out.println("Before: percent=" + percent);
        tripleValue(percent); //數值變量作爲參數傳入,無法被賦值 無法修改作用域以外的值
        System.out.println("After: percent="+percent);

        /**
         *  Test 2 :Method can change the state of object parameters
         *
         */

        System.out.println("\n Testing tripleSalary:");
        Employee harry = new Employee("Harry",50000);
        System.out.println("Before : salary = " + harry.getSalary());
        tripleSalary(harry); // 對象作爲參數傳入,可以被賦值 因爲對象調用了其本身的方法
        System.out.println("After  : salary = "+harry.getSalary());

        /**
         *  Test 3  :Methods cant attach new obj to obj parameters
         */

        System.out.println("\n Testing swap:");
        Employee a = new Employee("Alice",10000);
        Employee b = new Employee("Bob",20000);
        System.out.println("Before : a = "+a.getName());
        System.out.println("Before : b = "+b.getName());

        swap(a, b); // 只在方法內部實現了替換,但方法外部依然沒有被替換

        System.out.println("End : a = "+a.getName());
        System.out.println("End : b = "+b.getName());

    }

    public static void tripleValue(double x){
        x = 3*x;
        System.out.println("End of method : x="+x);
    }

    public static void tripleSalary(Employee x){
        x.raiseSalary(200);
        System.out.println("End of method:salary = "+x.getSalary());
    }

    public static void swap(Employee x,Employee y){
        Employee temp=x;
        x=y;
        y=temp;
        System.out.println("End of method: x = "+x.getName());
        System.out.println("End of method: y = "+y.getName());
    }


}
