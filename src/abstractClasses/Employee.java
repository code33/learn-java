package abstractClasses;

import java.util.Date;
import java.util.GregorianCalendar;

/**
 * ok
 * Created by jyo on 15-8-30.
 */
public class Employee extends Person {

    public Employee(String n, double s, int year, int mouth, int day) {
        super(n);
        salary = s;
        GregorianCalendar calendar = new GregorianCalendar(year, mouth - 1, day);
        hireDay = calendar.getTime();
    }


    public String getDescription() {
        return String.format("an employee with a salary of $%.2f", this.salary);
    }

    public static int getNextId() {
        return nextId;
    }

    public static void setNextId(int nextId) {
        Employee.nextId = nextId;
    }

    private static int nextId = 1;

    public int getId() {
        return id;
    }

    private int id;
    private String name;
    private double salary;
    private Date hireDay;


    public String getName() {
        return name;
    }

    public double getSalary() {
        return salary;
    }

    public Date getHireDay() {
        return hireDay;
    }


    public void raiseSalary(int up) {
        double raise = (salary * up) / 100;
        salary += raise;
    }

    public void setId() {
        id = nextId;
        nextId++;
    }


}
