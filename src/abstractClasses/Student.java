package abstractClasses;

/**
 * Created by jyo on 15-9-23.
 */
public class Student extends Person {

    public Student(String name) {
        super(name);
    }

    private String major;
    @Override
    public String getDescription() {
        return "a student majoring in "+this.major;
    }


//    @Override
//    public String getDescription() {
//        return super.getDescription();
//    }



}
