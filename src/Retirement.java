import java.util.Scanner;

/**
 *
 * Created by jyo on 15-8-28.
 */
public class Retirement {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("How much money do you need to retire");
        double goal = in.nextDouble();

        System.out.println("How much money will you contribute every year?");
        double payment = in.nextDouble();

        System.out.println("Interest rate in %:");
        double interestRate = in.nextDouble();

        double balance = 0;
        int years= 0 ;

        while (balance<goal){
            balance+= payment;
            double interrest = balance * interestRate / 100;
            balance+= interrest;
            years++;
        }
        System.out.printf("U can retire in %s years",years);
    }
}
