package EmployeeTest;

/**
 * Created by jyo on 15-9-22.
 */
public class ManagerTest {

    public static void main(String[] args) {
        Manager boss= new Manager("Carl Cracker",80000,1987,12,15);
        boss.setBonus(5000);

        Employee[] staff =new Employee[3];
        staff[0]=boss;
        staff[1]=new Employee("Harry Hacker",50000,1989,10,1);
        staff[2]=new Employee("Carl Cracker",40000,1990,3,15);
        for (Employee e: staff){
            e.raiseSalary(5);
            System.out.printf("name is %s,\n salary is %s \n, hireDay is %s \n \n", e.getName(), e.getSalary(), e.getHireDay());
        }
    }
}
