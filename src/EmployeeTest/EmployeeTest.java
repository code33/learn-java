package EmployeeTest;

/**
 *
 * Created by jyo on 15-8-30.
 */
public class EmployeeTest {

    public static void main (String[] args){
        Employee[] staff =new Employee[3];
        staff[0]=new Employee("Carl Cracker",75000,1987,12,15);
        staff[1]=new Employee("Harry Hacker",50000,1989,10,1);
        staff[2]=new Employee("Carl Cracker",40000,1990,3,15);

        for (Employee e: staff){
            e.raiseSalary(5);
            System.out.printf("name is %s,\n salary is %s \n, hireDay is %s \n \n", e.getName(), e.getSalary(), e.getHireDay());
        }

        Employee harry = new Employee("Harry",10000,1985,11,17);
        harry.setId();
        harry.getId();
        System.out.printf("getNextId:%s", Employee.getNextId());

        Employee.tripleSalary(harry);
//        System.out.printf("name is %s,\n salary is %s \n, hireDay is %s \n \n", harry.getName(), harry.getSalary(), harry.getHireDay());

    }
}
