package EmployeeTest;

import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by jyo on 15-8-30.
 */
class Employee {

    public static int getNextId() {
        return nextId;
    }

    public static void setNextId(int nextId) {
        Employee.nextId = nextId;
    }

    private static int nextId = 1;

    public int getId() {
        return id;
    }

    private int id;
    private String name;
    private double salary;
    private Date hireDay;

    public Employee(String n, double s, int year, int mouth, int day) {
        name = n;
        salary = s;
        GregorianCalendar calendar = new GregorianCalendar(year, mouth - 1, day);
        hireDay = calendar.getTime();
    }

    public String getName() {
        return name;
    }

    public double getSalary() {
        return salary;
    }

    public Date getHireDay() {
        return hireDay;
    }



    public void raiseSalary(int up) {
        double raise = (salary * up) / 100;
        salary += raise;
    }

    public void setId() {
        id = nextId;
        nextId++;
    }

    public static void tripleSalary(Employee x) {
        x.raiseSalary(200);
    }


}
