#learn-java

git.oschina.net 使用自定义key ssh 连接git

1.从邮箱下载 拷贝自己的公钥 和 私钥
 
2.放到 ～/.ssh 目录 下面

3.配置ssh 对应的连接设置文件 vim ~/.ssh/config

```

Host            git.oschina.net
HostName        git.oschina.net
User            git
IdentityFile    ~/.ssh/code33
IdentitiesOnly  yes

```

4.测试连接 ssh -T git.oschina.net

5.开始使用git

```
git init
git add remote url
git push branchname

```
