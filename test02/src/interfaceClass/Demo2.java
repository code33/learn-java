package interfaceClass;

import org.apache.log4j.PropertyConfigurator;

/**
 *
 * Created by jyo on 15-11-7.
 */
public class Demo2 {
    public static void main(String[] args) {

        ChinaKFC zhongshanlu = new ChinaKFC();
        System.out.println(zhongshanlu.getFood(1));
        System.out.println(zhongshanlu.toilet());
        System.out.println(zhongshanlu.meet());

    }
}

class Food{
    private String food;

    public Food(String food) {
        this.food = food;
    }

    public String getFood() {
        return food;
    }

    @Override
    public String toString() {
        return "Food{" +
                "food='" + food + '\'' +
                '}';
    }
}

interface KFC{
    Food  getFood(double money);
    String toilet();
}

interface dateSpace{
    String meet();
}

class ChinaKFC implements KFC,dateSpace{
    @Override
    public Food getFood(double money) {
        Food food;
        if(money >=5){
            food=new Food("冰淇淋");
        }else {
            food=new Food("纸巾一片");
        }
        return food;

    }

    @Override
    public String toilet() {
        return "只能嘘嘘";
    }

    @Override
    public String meet() {
        return "约会好地方";
    }
}