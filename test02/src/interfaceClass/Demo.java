package interfaceClass;

/**
 * Created by jyo on 15-11-7.
 */
import static java.lang.System.*;

public class Demo{
    public static void main(String[] args) {
        SataHdd sh1=new SeagateHdd(); //初始化希捷硬盘
        SataHdd sh2=new SamsungHdd(); //初始化三星硬盘
        sh1.writeData("Somthing");
        sh1.readData();
        sh1.writeData("Some wrong");
        sh2.readData();
        fixHdd sghd=new SeagateHdd();
        sghd.doFix();

//        System.out.println("sh1:"+sh1);
//        System.out.println("sh2:"+sh2);
    }
}




//串行硬盘接口
interface SataHdd{
    //连接线的数量
    public static final int CONNECT_LINE=4;
    //写数据
    public void writeData(String data);
    //读数据
    public String readData();
}

// 维修硬盘接口
interface fixHdd{
    // 维修地址
    String address = "北京市海淀区";
    // 开始维修
    boolean doFix();
}

//希捷硬盘
class SeagateHdd implements SataHdd, fixHdd{
    //希捷硬盘读取数据
    public String readData(){
        return "数据";
    }
    //希捷硬盘写入数据
    public void writeData(String data) {
        out.println("数据:"+data+"...\n写入成功!");
    }
    // 维修希捷硬盘
    public boolean doFix(){
        System.out.println("维修"+address);
        return true;
    }
}
//三星硬盘
class SamsungHdd implements SataHdd{
    //三星硬盘读取数据
    public String readData(){
        return "数据";
    }
    //三星硬盘写入数据
    public void writeData(String data){
        out.println("写入成功");
    }
}
//某劣质硬盘，不能写数据
abstract class XXHdd implements SataHdd{
    //硬盘读取数据
    public String readData() {
        return "数据";
    }
}