package genericsClass;

import org.apache.log4j.PropertyConfigurator;

import java.util.List;

/**
 *
 * Created by jyo on 15-11-7.
 */


public class genericRestric {
    public static void main(String[] args){
        Point2<Integer,Double> p = new Point2<Integer,Double>();  // 类型擦除
//        Point2 p =new Point2();
        p.setX(10);
        p.setY(20.8);
        int x = (Integer) p.getX();  // 向下转型
        double y = (Double) p.getY();
        System.out.println("This point is：" + x + ", " + y);

        //这里不能使用int float 基本类型 无法调用Number对象内的方法
        //Double Integer 为对象型 继承於Number 是对 int float的封装
        Integer[] ary={1,3,5};
        int a=p.getMax(ary);
        System.out.println("maxNum:"+a);
        Point3[] p3={new Point3(10)};

        Point3 b=p.getMin(p3);
        System.out.println("b:"+b);
    }
}

class Point3<R> implements pp<R>,qq<R>{

    private int age;
    public Point3(int age) {
        this.age=age;
    }

    @Override
    public String toString() {
        return "Point3{" +
                "age=" + age +
                '}';
    }

    @Override
    public String create(String str) {
        return null;
    }

    @Override
    public R save(boolean result) {
        return null;
    }


}

interface pp<T>{
    String create(String str);
    T save(boolean result);
}

interface qq<T>{
    String create(String str);
    T save(boolean result);
}

class Point2<T1, T2>{
    T1 x;
    T2 y;
    public T1 getX() {
        return x;
    }
    public void setX(T1 x) {
        this.x = x;
    }
    public T2 getY() {
        return y;
    }
    public void setY(T2 y) {
        this.y = y;
    }

    public <T extends Number >T getMax(T arra[]){
        T max = null;
        for(T element : arra){
//            max = element.doubleValue() > max.doubleValue() ? element : max;
//          对初始化为Null的变量 若要调用其形参方法 需要对其为null与否进行判定
            max = (((max != null) ? max.doubleValue() : 0) < element.doubleValue()) ? element : max;
        }
        return max;

    }

    public  <R extends pp & qq> R getMin(R arra[]){
        R min=arra[0];
        return min;
    }
}