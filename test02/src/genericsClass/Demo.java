package genericsClass;

/**
 * Created by jyo on 15-11-7.
 */
public class Demo {
    public static void main(String[] args) {
        // 实例化泛型类
        Point<Integer, Integer> p1 = new Point<Integer, Integer>();
        p1.setX(10);
        p1.setY(20);
        int x = p1.getX();
        int y = p1.getY();
        System.out.println("This point is：" + x + ", " + y);

        Point<Double, String> p2 = new Point<Double, String>();
        p2.setX(25.4);
        p2.setY("东京180度");
        double m = p2.getX();
        String n = p2.getY();
        System.out.println("This point is：" + m + ", " + n);
    }
}

// 定义泛型类
class Point<T1, T2> {
    T1 x;
    T2 y;

    // 返回T1型的get函数
    public T1 getX() {
        return x;
    }

    // 用T1型的对象进行赋值
    public void setX(T1 x) {
        this.x = x;
    }

    public T2 getY() {
        return y;
    }

    public void setY(T2 y) {
        this.y = y;
    }
}