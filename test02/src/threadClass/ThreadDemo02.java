package threadClass;

import org.apache.log4j.PropertyConfigurator;

/**
 *
 * Created by jyo on 15-11-8.
 */

class Person2 implements Runnable {
  private String name;

  public Person2(String name) {
    this.name = name;
  }

  @Override
  public void run() {
    for(int i = 0; i < 50; i++) {
      try {
        Thread.sleep(1000);
        System.out.println("name:" + name + ",i=" + i);
      } catch(InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}

public class ThreadDemo02 {
  public static void main(String[] args) {
    String log4jConfPath = "log4j.properties";
    PropertyConfigurator.configure(log4jConfPath);
    Person2 pp = new Person2("A");
    Thread t = new Thread(pp);
    t.start();
    // 线程强行插入
    for(int i = 0; i < 50; i++) {
      if(i > 10) {
        try {
          t.join();
        } catch(InterruptedException e) {
          e.printStackTrace();
        }
      }

      System.out.println("我是主线程" + i);
    }
  }
}


