package threadClass;

import org.apache.log4j.PropertyConfigurator;

/**
 *
 * Created by jyo on 15-11-8.
 */

class ThRun implements Runnable{

  @Override
  public void run() {
    for(int i = 0; i < 50; i++) {
      try {
        Thread.sleep(1000);
        System.out.println("ThreadName:" +Thread.currentThread().getName()+";i="+i);
      } catch(InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}

public class ThreadDemo04 {
  public static void main(String[] args) {
    String log4jConfPath = "log4j.properties";
    PropertyConfigurator.configure(log4jConfPath);
    Thread t1 = new Thread(new ThRun(),"A");
    Thread t2 = new Thread(new ThRun(),"B");
    Thread t3 = new Thread(new ThRun(),"C");
    t1.setPriority(Thread.MIN_PRIORITY);
    t2.setPriority(Thread.NORM_PRIORITY);
    t3.setPriority(Thread.MAX_PRIORITY);
    t1.start();
    t2.start();
    t3.start();
  }
}
