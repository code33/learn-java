package threadClass;

import org.apache.log4j.PropertyConfigurator;

/**
 * Created by jyo on 15-11-8.
 */

class Person3 implements Runnable {
  private String name;

  public Person3(String name) {
    this.name = name;
  }

  @Override
  public void run() {
    for(int i = 0; i < 500; i++) {
      System.out.println("name:" + name + " ,i=" + i);
      // 通过判定条件,线程资源让出来,给领导先走!
      if(i % 10 == 0) {
        //我是领导
        System.out.println("领导:"+name+ "~~都停下唉~我要装逼了!" + i);
        Thread.yield();
      }
    }
  }
}

public class ThreadDemo03 {
  public static void main(String[] args) {
    String log4jConfPath = "log4j.properties";
    PropertyConfigurator.configure(log4jConfPath);
    Person3 pA=new Person3("A");
    Person3 pB=new Person3("B");
    Thread t1=new Thread(pA);
    Thread t2=new Thread(pB);
    t2.start();

    t1.start();

  }
}
