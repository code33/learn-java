package threadClass;

import org.apache.log4j.PropertyConfigurator;

/**
 * 同步与死锁
 * synchronized 关键字
 * 代码块和方法都可以同步
 * 资源需要共享的时候 使用同步
 * Created by jyo on 15-11-8.
 */

class MyThreadDemo implements Runnable {

  private int ticket = 50;

  public synchronized void tell() {
    if(ticket > 0) {
//      try {
//        Thread.sleep(500);
        System.out.println("车票还有" + (ticket--) + "张"+",位置:"+Thread.currentThread().getName());
//      } catch(InterruptedException e) {
//        e.printStackTrace();
//      }

    }

  }

  @Override
  public void run() {
    for(int i = 0; i < 60; i++) {
      tell();
    }
  }
}

public class ThreadDemo05 {
  public static void main(String[] args) {
    String log4jConfPath = "log4j.properties";
    PropertyConfigurator.configure(log4jConfPath);
    MyThreadDemo m = new MyThreadDemo();
    Thread t1 = new Thread(m, "窗口1");
    Thread t2 = new Thread(m, "窗口2");
    Thread t3 = new Thread(m, "窗口3");
    t1.setPriority(10);
    t3.setPriority(1);
    t3.start();
    t1.start();
    t2.start();
  }
}
