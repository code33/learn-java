package threadClass;

import org.apache.log4j.PropertyConfigurator;

/**
 *
 * Created by jyo on 15-11-8.
 */

class Person implements Runnable{
    private String name;

    public Person(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            System.out.println("name:" +name+",i="+i);
        }
    }
}

public class ThreadDemo01 {
    public static void main(String[] args) {
        String log4jConfPath = "log4j.properties";
        PropertyConfigurator.configure(log4jConfPath);
        Person p1 = new Person("A");
        Person p2 = new Person("B");
        Thread t1= new Thread(p1);
        Thread t2 = new Thread(p2);
        t2.start();
        t1.start();
    }
}

