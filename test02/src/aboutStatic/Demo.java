package aboutStatic;

/**
 * Created by jyo on 15-11-7.
 */
public class Demo {
    static int i = 10;
    int j;

    Demo() {
        this.j = 20;
    }

    public static void main(String[] args) {
        System.out.println("类变量 i=" + Demo.i);
        Demo obj = new Demo();
        System.out.println("实例变量 j=" + obj.j);
    }
}


