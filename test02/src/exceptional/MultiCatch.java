package exceptional;

/**
 * Created by jyo on 15-11-7.
 */
class MultiCatch {
    public static void main(String args[]) {
        try {
            int a = args.length;
            System.out.println("a = " + a);
            int b = 42 / a;
            int[] c= {1};
            c[42] = 99;
        // 捕获一个被除数为0的异常
        } catch(ArithmeticException e) {
            System.out.println("Divide by 0: " + e);
        // 捕获一个错误数组索引的异常
        } catch(ArrayIndexOutOfBoundsException e) {
            System.out.println("Array index oob: " + e);
        }
        System.out.println("After try/catch blocks.");
    }
}
