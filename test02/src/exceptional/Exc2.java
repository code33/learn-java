package exceptional;

/**
 * Created by jyo on 15-11-7.
 */

class Exc2 {
    public static void main(String args[]) {
        //如果不作异常捕获 程序就会退出并输出异常 :
        //java.lang.ArithmeticException: / by zero
        int d, a;
        /*

        d=0;
        a=10/d;

        System.out.println("a:"+a);
        */

        try { // monitor a block of code.
            d = 1;
            a = 42 / d;
            System.out.println("This will not be printed.");
        // 所以这里就需要捕获 ArithmeticException这种类型的异常
        } catch (ArithmeticException e) { // catch divide-by-zero error
            System.out.println("Division by zero.");
        }
        System.out.println("After catch statement.");
    }
}
