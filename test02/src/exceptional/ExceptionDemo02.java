package exceptional;

import org.apache.log4j.PropertyConfigurator;

/**
 * Created by jyo on 15-11-8.
 */
public class ExceptionDemo02 {
    public static void main(String[] args) {
        String log4jConfPath = "log4j.properties";
        PropertyConfigurator.configure(log4jConfPath);
        try{
            tell(10,0);
        }catch (ArithmeticException e){
            System.out.println("e:" +e);
        }

    }
    public static void tell(int i,int j) throws ArithmeticException{
        int tmp=0;
        tmp=i/j;
        System.out.println("tmp:" +tmp);
    }
}
