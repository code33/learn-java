package exceptional;

import org.apache.log4j.PropertyConfigurator;

/**
 *
 * Created by jyo on 15-11-7.
 */

class Exc{
    int a=10,b=10;
}

public class ExceptionDemo01 {
    public static void main(String[] args) {
        String log4jConfPath = "log4j.properties";
        PropertyConfigurator.configure(log4jConfPath);
        int tmp=0;

        Exc er= null;
        er=new Exc();

        try{
            tmp = er.a/er.b;
        }catch (NullPointerException e){
            System.out.println("e1:"+e);
        }catch (ArithmeticException e2){
            System.out.println("e2:"+e2);
        }finally {
            System.out.println("tmp:" +tmp);
            System.out.println("程序退出");
        }



    }
}
