package exceptional;

import java.util.Random;

/**
 *
 * Created by jyo on 15-11-7.
 */
class HandleError {

    public static void main(String args[]) {
        int a=0, b=0, c=0;
        Random r = new Random();
        // 循环执行32000次
        for(int i=0; i<32000; i++) {
            try {
                b = r.nextInt();
                c = r.nextInt();
                a = 12345 / (b/c);
            } catch (ArithmeticException e) {
                //  对异常的捕获,都需要辅以日志跟踪处理
                System.out.println("Division by zero.");
                // 对a重新初始化 ,以便循环得以继续
                a = 0; // set a to zero and continue
            }
            System.out.println("a: " + a+" i:"+i);
        }
    }
}