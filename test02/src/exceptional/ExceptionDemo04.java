package exceptional;

import org.apache.log4j.PropertyConfigurator;

/**
 *
 * Created by jyo on 15-11-8.
 */

class MyException extends Exception{
    public MyException(String message) {
        super(message);
    }

}

public class ExceptionDemo04 {
    public static void main(String[] args) {
        String log4jConfPath = "log4j.properties";
        PropertyConfigurator.configure(log4jConfPath);
        try {
            throw new MyException("自定义异常");
        } catch (Exception e) {
            System.out.println("e:" +e);
        }
    }
}
