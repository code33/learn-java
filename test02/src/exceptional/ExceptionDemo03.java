package exceptional;

import org.apache.log4j.PropertyConfigurator;

/**
 * Created by jyo on 15-11-8.
 */
public class ExceptionDemo03 {
    public static void main(String[] args) {
        String log4jConfPath = "log4j.properties";
        PropertyConfigurator.configure(log4jConfPath);
        try {
            throw new Exception("实例化异常");
        } catch (Exception e) {
            System.out.println("e:" +e);
        }
    }
}
