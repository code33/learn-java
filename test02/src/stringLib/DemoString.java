package stringLib;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * Created by jyo on 15-11-7.
 */
public class DemoString {
    public static void main(String[] args) throws IOException {
        String log4jConfPath = "log4j.properties";
        org.apache.log4j.PropertyConfigurator.configure(log4jConfPath);

        try {
            BufferedReader br = new BufferedReader(new FileReader("file.txt"));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
            String everything = sb.toString();
            System.out.println("everythins:"+everything);
            br.close();
        }
        catch(IOException e) {
            e.printStackTrace();
            System.out.println("IO problem");
        }
        finally {
            System.out.println("finally");
        }
    }
}
